<?php

use App\Models\LoggedInUser;
use Carbon\Carbon;

function hashEncrypt($payload): string
{
    $salt = "!mysecretkey#9^5usdk39d&dlf)03sL";

    $payloadKey = $payload.$salt;
    return hash_hmac("sha256", $payloadKey, $salt);
}

function formatDate($date) {
    return Carbon::parse($date)->format("D, M d, 'y");
}

function setLoggedInUser($data, $webId) {
    $loggedInUser = new LoggedInUser();
    $loggedInUser->setNip($data['user']['nip']);
    $loggedInUser->setName($data['user']['name']);
    $loggedInUser->setAccesToken($data['access_token']);
    $loggedInUser->setProfilePhotoUrl($data['user']['profile_photo_url']);
    $loggedInUser->setUserType($data['user']['user_type']);
    $loggedInUser->setWebId($webId);

    session()->put('loggedInUser', $loggedInUser);
}
