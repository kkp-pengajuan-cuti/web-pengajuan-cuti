<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use stdClass;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $apiUrl = env("API_URL");

        $nip = $request->input("nip");
        $password = $request->input("password");

        $loggedInUser = session('loggedInUser');

        $webId = Str::uuid();

        if ($loggedInUser != null){
            if ($loggedInUser->getWebId() != null) {
                $webId = $loggedInUser->getWebId();
            }
        }

        $payload = new stdClass();
        $payload->nip = $nip;
        $payload->web_id = $webId;
//        $payload->grant_type = "password";
//        $payload->client_id = env("CLIENT_ID");
//        $payload->client_secret = env("CLIENT_SECRET");
//        $payload->username = $nip;
//        $payload->password = $password;
//        $payload->scope = "*";

        $hash = hashEncrypt(json_encode($payload));

        $response = Http::acceptJson()->withHeaders([
            'hash' => $hash,
        ])->post($apiUrl . "oauth/token",[
            "grant_type" => "password",
            "client_id" => env("CLIENT_ID"),
            "client_secret" => env("CLIENT_SECRET"),
            "username" => $nip,
            "password" => $password,
            "scope" => "*",
            "payload" => json_encode($payload)
        ]);

        if ($response->successful()) {
            $data = $response['data'];
            setLoggedInUser($data, $webId);
            return redirect('verification');
        }else{
            return redirect('login')->with('status', 'Login Failed');
        }
    }
}
