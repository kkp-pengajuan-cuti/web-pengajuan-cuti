<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use stdClass;

class RegisterController extends Controller
{
    const NOT_FOUND = 404;

    public function index()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $apiUrl = env('API_URL');
        $webId = Str::uuid();

        $registerResponse = Http::acceptJson()->withHeaders([
            'hash' => $this->getHash($webId, $request)
        ])->post($apiUrl . "user/register", ['payload' => $this->getHash($webId, $request, true)]);

        if ($registerResponse->successful()) {
            $payload = new stdClass();
            $payload->nip = $request->input('nip');
            $payload->web_id = $webId;
            $oauthResponse = Http::acceptJson()->withHeaders([
                'hash' => hashEncrypt(json_encode($payload)),
            ])->post($apiUrl . "oauth/token", [
                "grant_type" => "password",
                "client_id" => env("CLIENT_ID"),
                "client_secret" => env("CLIENT_SECRET"),
                "username" => $request->input('nip'),
                "password" => $request->input('password'),
                "scope" => "*",
                "payload" => json_encode($payload)
            ]);

            if ($oauthResponse->successful()) {
                $data = $oauthResponse['data'];
                setLoggedInUser($data, $webId);
                return redirect('verification');
            } else {
                return back()->with('status', "Register failed");
            }
        } else {
            if ($registerResponse->status() == self::NOT_FOUND) {
                return back()->with('status', "Nip not found");
            } else {
                return back()->with('status', $registerResponse['message']);
            }
        }
    }

    private function getHash($webId, $request, $payloadState = false)
    {
        $payload = new stdClass();
        $payload->nip = (int)$request->input('nip');
        $payload->email = $request->input('email');
        $payload->password = $request->input('password');
        $payload->web_id = $webId;
        if ($payloadState == true) {
            return json_encode($payload);
        }else{
            return hashEncrypt(json_encode($payload));
        }
    }
}
