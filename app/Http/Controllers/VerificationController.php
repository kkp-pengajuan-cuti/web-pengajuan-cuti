<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use stdClass;

class VerificationController extends Controller
{
    public function index()
    {
        return view('verification');
    }

    public function check(Request $request)
    {
        $apiUrl = env("API_URL");

        $loggedInUser = session()->get('loggedInUser');

        $payload = new stdClass();
        $payload->nip = $loggedInUser->getNip();
        $payload->code_web = $request->input('code');
        $payload->web_id = $loggedInUser->getWebId();
        $payload->is_register = 0;

        $response = Http::acceptJson()->withHeaders([
            'hash' => hashEncrypt(json_encode($payload))
        ])->post($apiUrl . 'verification/verify',['payload' => json_encode($payload)]);
        if ($response->successful()) {
            return redirect('/');
        } else {
            return match ($response->status()) {
                401 => redirect('verification')
                    ->with('status', "The verification code you have entered is invalid. Please enter a valid verification code"),
                400 => redirect('verification')
                    ->with('status', "The verification code you have entered is expired. Please resend code and enter a valid verification code"),
                default => redirect('verification')
                    ->with('status', 'Verification code failed'),
            };
        }
    }
}
