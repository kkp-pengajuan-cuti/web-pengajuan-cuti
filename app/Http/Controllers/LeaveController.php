<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use stdClass;

class LeaveController extends Controller
{
    const UNAUTHORIZED = 401;

    public function index()
    {
        $apiUrl = env("API_URL");

        $response = Http::acceptJson()->get($apiUrl . "hrd");

        $listDataHrd = [];

        if ($response->successful()){
            $listDataHrd = $response['data'];
        }

        return view('createleave')
            ->with('listDataHrd', $listDataHrd)
            ->with('pageName', "Create Leave");
    }

    public function create(Request $request)
    {
        $apiUrl = env("API_URL");

        // if access token null then do log out process
        $loggedInUser = session('loggedInUser');
        if ($loggedInUser == null) return redirect('login');

        $accessToken = $loggedInUser->getAccesToken();

        $payload = new stdClass();
        $payload->nip = $loggedInUser->getNip();
        $payload->web_id = $loggedInUser->getWebId();
        $payload->subject = $request->input('subject');
        $payload->description = $request->input('description');
        $payload->start_date = $request->input('start_date');
        $payload->end_date = $request->input('end_date');
        $payload->request_to = $request->input('request_to');
        $payload->type_id = $request->input('type_id');

        $hash = hashEncrypt(json_encode($payload));

        $response = Http::acceptJson()->withHeaders([
            'hash' => $hash
        ])->withToken($accessToken)->post($apiUrl . "leave/create", ['payload' => json_encode($payload)]);

        if ($response->status() == self::UNAUTHORIZED){
            return redirect('login')->with('status', "Unauthorized");
        }

        if ($response->successful()){
            return redirect('/');
        }
    }
}
