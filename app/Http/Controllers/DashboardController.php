<?php

namespace App\Http\Controllers;

use Illuminate\Http\Client\Pool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use stdClass;

class DashboardController extends Controller
{
    const UNAUTHORIZED = 401;

    public function index()
    {
        $apiUrl = env("API_URL");

        // if access token null then do log out process
        $loggedInUser = session('loggedInUser');
        if ($loggedInUser == null) return redirect('login');

        $accessToken = $loggedInUser->getAccesToken();

        $hashAwaiting = $this->getHash($loggedInUser, 1);
        $hashDeclined = $this->getHash($loggedInUser, 2);
        $hashApproved = $this->getHash($loggedInUser, 3);

        $responses = Http::acceptJson()->pool(fn (Pool $pool) => [
                $pool->as('awaiting')->withHeaders(['hash' => $hashAwaiting])
                    ->withToken($accessToken)
                    ->get($apiUrl . "leave",['payload' => $this->getHash($loggedInUser, 1, null, null, true)]),
                $pool->as('declined')->withHeaders(['hash' => $hashDeclined])
                    ->withToken($accessToken)
                    ->get($apiUrl . "leave", ['payload' => $this->getHash($loggedInUser, 2, null, null, true)]),
                $pool->as('approved')->withHeaders(['hash' => $hashApproved])
                    ->withToken($accessToken)
                    ->get($apiUrl . "leave", ['payload' => $this->getHash($loggedInUser, 3, null, null, true)])
        ]);

        if ($responses['awaiting']->status() == self::UNAUTHORIZED){
            return redirect('login')->with('status', "Unauthorized");
        }

        if ($responses['awaiting']->ok()){
            $data = [
                "awaiting" => $responses['awaiting']['data'],
                "declined" => $responses['declined']['data'],
                "approved" => $responses['approved']['data'],
            ];
            return view('dashboard')->with("data", $data)
                ->with('pageName', "Dashboard");

        }else{
            return redirect('login');
        }
    }

    public function reject(Request $request)
    {
        $apiUrl = env("API_URL");

        // if access token null then do log out process
        $loggedInUser = session('loggedInUser');
        if ($loggedInUser == null) return redirect('login');

        $accessToken = $loggedInUser->getAccesToken();

        $id = $request->input('id');
        $reason = $request->input('reason');

        $hash = $this->getHash($loggedInUser, null, $id, $reason);

        $response = Http::acceptJson()->withHeaders([
            'hash' => $hash
        ])->withToken($accessToken)->post($apiUrl . "leave/reject",
            ['payload' => $this->getHash($loggedInUser, null, $id, $reason, true)]);

        if ($response->status() == self::UNAUTHORIZED){
            return redirect('login')->with('status', "Unauthorized");
        }

        return back();
    }

    public function approve(Request $request)
    {
        $apiUrl = env("API_URL");

        // if access token null then do log out process
        $loggedInUser = session('loggedInUser');
        if ($loggedInUser == null) return redirect('login');

        $accessToken = $loggedInUser->getAccesToken();

        $id = $request->input('id');

        $hash = $this->getHash($loggedInUser, null, $id, null);

        $response = Http::acceptJson()->withHeaders([
            'hash' => $hash
        ])->withToken($accessToken)->post($apiUrl . "leave/approve",['payload' => $this->getHash($loggedInUser, null, $id, null, true)]);

        if ($response->status() == self::UNAUTHORIZED){
            return redirect('login')->with('status', "Unauthorized");
        }

        return back();
    }

    public function logout(Request $request)
    {
        $apiUrl = env('API_URL');

        // if access token null then do log out process
        $loggedInUser = session('loggedInUser');
        if ($loggedInUser == null) return redirect('login');

        $accessToken = $loggedInUser->getAccesToken();

        $payload = new stdClass();
        $payload->web_id = $loggedInUser->getWebId();

        $response = Http::acceptJson()->withHeaders([
            'hash' => hashEncrypt(json_encode($payload))
        ])->withToken($accessToken)->post($apiUrl . "auth/logout", ['payload' => json_encode($payload)]);

        if ($response->successful()){
            $request->session()->invalidate();
            return redirect('login');
        }
    }

    private function getHash($loggedInUser, $status = null, $id = null, $reason = null, $payloadState = false): string
    {
        $payload = new stdClass();
        $payload->id = $id;
        $payload->nip = $loggedInUser->getNip();
        $payload->web_id = $loggedInUser->getWebId();
        $payload->user_type = $loggedInUser->getUserType();
        $payload->reason = $reason;

        if ($id != null){
            if ($payloadState == true){
                return json_encode($payload);
            }else{
                return hashEncrypt(json_encode($payload));
            }
        }else{
            $payload->status = $status;
            if ($payloadState == true) {
                return json_encode($payload);
            }else{
                return hashEncrypt(json_encode($payload));
            }
        }
    }

}
