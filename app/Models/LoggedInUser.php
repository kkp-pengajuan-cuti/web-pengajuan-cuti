<?php

namespace App\Models;

class LoggedInUser {

    private $nip;
    private $name;
    private $accesToken;
    private $profilePhotoUrl;
    private $userType;
    private $webId;

    /**
     * @return mixed
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param mixed $nip
     */
    public function setNip($nip): void
    {
        $this->nip = $nip;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAccesToken()
    {
        return $this->accesToken;
    }

    /**
     * @param mixed $accesToken
     */
    public function setAccesToken($accesToken): void
    {
        $this->accesToken = $accesToken;
    }

    /**
     * @return mixed
     */
    public function getProfilePhotoUrl()
    {
        return $this->profilePhotoUrl;
    }

    /**
     * @param mixed $profilePhotoUrl
     */
    public function setProfilePhotoUrl($profilePhotoUrl): void
    {
        $this->profilePhotoUrl = $profilePhotoUrl;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType): void
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getWebId()
    {
        return $this->webId;
    }

    /**
     * @param mixed $webId
     */
    public function setWebId($webId): void
    {
        $this->webId = $webId;
    }


}
