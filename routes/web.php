<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$HEADER = [
    'profileName' => 'Ahmad Sopian',
    'email' => 'sopianahmad120@gmail.com',
    'name' => 'Dashboard'
];

Route::get('login', function () {
    return view('login');
});
Route::post('login',[\App\Http\Controllers\LoginController::class, 'login']);
Route::post('logout',[\App\Http\Controllers\DashboardController::class, 'logout']);

Route::get('register', [\App\Http\Controllers\RegisterController::class, 'index']);
Route::post('register', [\App\Http\Controllers\RegisterController::class, 'register']);

Route::get('verification', [\App\Http\Controllers\VerificationController::class, 'index']);
Route::post('verification', [\App\Http\Controllers\VerificationController::class, 'check']);

Route::get('/', [\App\Http\Controllers\DashboardController::class, 'index']);
Route::post('leave/reject', [\App\Http\Controllers\DashboardController::class, 'reject']);
Route::post('leave/approve', [\App\Http\Controllers\DashboardController::class, 'approve']);

Route::post('leave/create', [\App\Http\Controllers\LeaveController::class, 'create']);
Route::get('leave/create', [\App\Http\Controllers\LeaveController::class, 'index']);


