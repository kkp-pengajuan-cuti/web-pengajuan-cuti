<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="" />
<meta name="author" content="" />
<meta name="robots" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">

<!-- PAGE TITLE HERE -->
<title>Workload Project Management</title>

<!-- FAVICONS ICON -->
<link rel="shortcut icon" type="image/png" href="{!! asset('images/CPLogo 2.png') !!}" />
@section('style')
@show
<link href="{!! asset('theme/css/style.css') !!}" rel="stylesheet">
