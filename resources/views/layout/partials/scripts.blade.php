<!--**********************************
    Scripts
***********************************-->
<!-- Required vendors -->
<script src="{!! asset('theme/vendor/global/global.min.js') !!}"></script>

@section('script')
@show

<script src="{!! asset('theme/js/custom.min.js') !!}"></script>
<script src="{!! asset('theme/js/dlabnav-init.js') !!}"></script>
