<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.head')
</head>
<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="lds-ripple">
        <div></div>
        <div></div>
    </div>
</div>
<!--*******************
    Preloader end
********************-->

<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">
    @include('layout.partials.nav')
    @include('layout.partials.header')
    @include('layout.partials.sidebar')
    @yield('content')
</div>
<!--**********************************
    Main wrapper end
***********************************-->

@include('layout.partials.scripts')
</body>
</html>
