@extends('layout.main')
@section('content')
    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Leave Application</h4>
                            @if(session('loggedInUser')->getUserType() == "karyawan")
                                <a  href="{{ url('leave/create') }}" class="btn btn-primary btn-sm"><i class="fas fa-clipboard-list"></i></a>
                            @endif
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-pills mb-4 light">
                                <li class=" nav-item">
                                    <a href="#navpills-1" class="nav-link active" data-bs-toggle="tab"
                                       aria-expanded="false">Awaiting</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills-2" class="nav-link" data-bs-toggle="tab" aria-expanded="false">Declined</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#navpills-3" class="nav-link" data-bs-toggle="tab" aria-expanded="true">Approved</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="navpills-1" class="tab-pane active">
                                    <div class="row">
                                        @foreach($data['awaiting'] as $awaiting)
                                            <div class="col-xl-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between">
                                                            <h5 class="d-inline mb-0">
                                                                {{ $awaiting['subject'] }}
                                                            </h5>
                                                            <span class="badge light badge-warning">Awaiting</span>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="mb-0">{{ $awaiting['type'] }}</p>
                                                                <span class="text-muted">Leave from : </span><span>{{ formatDate($awaiting['start_date']) }} - {{ formatDate($awaiting['end_date']) }}</span><br>
                                                                @if(session('loggedInUser')->getUserType() == "karyawan")
                                                                    <span class="text-muted">HRD : </span>
                                                                    <span>{{ $awaiting['hrd'] }}</span><br>
                                                                @else
                                                                    <span class="text-muted">Employee : </span>
                                                                    <span>{{ $awaiting['employee'] }}</span><br>
                                                                @endif
                                                                <span
                                                                    class="text-muted fst-italic">"{{ $awaiting['description'] }}"</span>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex justify-content-around">
                                                            @if(session('loggedInUser')->getUserType() == "hrd")
                                                                <button type="button" class="btn btn-danger btn-md"
                                                                        data-bs-toggle="modal" data-bs-target="#rejectModal">
                                                                    Reject
                                                                </button>
                                                                <button type="button" class="btn btn-primary btn-md"
                                                                        data-bs-toggle="modal" data-bs-target="#approveModal">
                                                                    Approve
                                                                </button>
                                                                <div class="modal fade" id="rejectModal">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title">Alert</h5>
                                                                                <button type="button" class="btn-close" data-bs-dismiss="modal">
                                                                                </button>
                                                                            </div>
                                                                            <form method="post" action="{{ url("leave/reject") }}">
                                                                                @csrf
                                                                                <div class="modal-body">
                                                                                    <p>Are you sure?</p>
                                                                                    <input type="hidden" name="id" value="{{ $awaiting['id'] }}">
                                                                                    <input class="form-control form-control-sm"
                                                                                           type="text" name="reason" placeholder="Reason">
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            class="btn btn-danger light"
                                                                                            data-bs-dismiss="modal">Close
                                                                                    </button>
                                                                                    <input type="submit"
                                                                                           class="btn btn-primary">
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal fade" id="approveModal">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title">Alert</h5>
                                                                                <button type="button" class="btn-close" data-bs-dismiss="modal">
                                                                                </button>
                                                                            </div>
                                                                            <form method="post" action="{{ url("leave/approve") }}">
                                                                                @csrf
                                                                                <div class="modal-body">
                                                                                    <p class="mb-0">Are you sure?</p>
                                                                                    <input type="hidden" name="id" value="{{ $awaiting['id'] }}">
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            class="btn btn-danger light"
                                                                                            data-bs-dismiss="modal">Close
                                                                                    </button>
                                                                                    <input type="submit"
                                                                                           class="btn btn-primary">
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="navpills-2" class="tab-pane">
                                    <div class="row">
                                        @foreach($data['declined'] as $declined)
                                            <div class="col-xl-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between">
                                                            <h5 class="d-inline mb-0">
                                                                {{ $declined['subject'] }}
                                                            </h5>
                                                            <span class="badge light badge-danger">Declined</span>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="mb-0">{{ $declined['type'] }}</p>
                                                                <span class="text-muted">Leave from : </span><span>{{ formatDate($declined['start_date']) }} - {{ formatDate($declined['end_date']) }}</span><br>
                                                                @if(session('loggedInUser')->getUserType() == "karyawan")
                                                                    <span class="text-muted">HRD : </span>
                                                                    <span>{{ $declined['hrd'] }}</span><br>
                                                                @else
                                                                    <span class="text-muted">Employee : </span>
                                                                    <span>{{ $declined['employee'] }}</span><br>
                                                                @endif
                                                                <span
                                                                    class="text-muted fst-italic">"{{ $declined['description'] }}"</span><br>
                                                                <p class="mb-0">Rejected</p>
                                                                <p class="fst-italic text-red">
                                                                    "{{ $declined['reject_reason'] }}"</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="navpills-3" class="tab-pane">
                                    <div class="row">
                                        @foreach($data['approved'] as $approved)
                                            <div class="col-xl-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="d-flex justify-content-between">
                                                            <h5 class="d-inline mb-0">
                                                                {{ $approved['subject'] }}
                                                            </h5>
                                                            <span class="badge light badge-primary">Approved</span>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="mb-0">{{ $approved['type'] }}</p>
                                                                <span class="text-muted">Leave from : </span><span>{{ formatDate($approved['start_date']) }} - {{ formatDate($approved['end_date']) }}</span><br>
                                                                @if(session('loggedInUser')->getUserType() == "karyawan")
                                                                    <span class="text-muted">HRD : </span>
                                                                    <span>{{ $approved['hrd'] }}</span><br>
                                                                @else
                                                                    <span class="text-muted">Employee : </span>
                                                                    <span>{{ $approved['employee'] }}</span><br>
                                                                @endif
                                                                <span
                                                                    class="text-muted fst-italic">"{{ $approved['description'] }}"</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
@endsection

@section('script')
    <script src="{!! asset('theme/js/bootstrap.bundle.min.js') !!}"></script>
    <script>

        $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
            console.log("tab shown...");
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });

        // read hash from page load and change tab
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('.nav-pills a[href="' + activeTab + '"]').tab('show');
        }

    </script>
@endsection
