@extends('layout.main')
@section('content')
    <div class="content-body">
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="basic-form">
                                <form action="{{ url('leave/create') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label">Type</label>
                                            <select class="default-select form-control wide" name="type_id">
                                                <option selected="">Type...</option>
                                                <option value="1">Sick Leave</option>
                                                <option value="2">Casual Leave</option>
                                                <option value="3">Annual Leave</option>
                                            </select>
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label">Cause</label>
                                            <input type="text" class="form-control" placeholder="Cause" name="subject">
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label">Description</label>
                                            <textarea class="form-control" placeholder="Description" name="description"></textarea>
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label>Reporting HRD</label>
                                            <select class="default-select form-control wide" name="request_to">
                                                <option selected="">Reporting HRD...</option>
                                                @foreach($listDataHrd as $val)
                                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label>Start Date</label>
                                            <input type="text" id="start" class="form-control" name="start_date">
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label>End Date</label>
                                            <input type="text" id="end" class="form-control" name="end_date">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{!! asset('theme/vendor/jquery-nice-select/css/nice-select.css') !!}" rel="stylesheet">
    <link href="{!! asset('theme/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') !!}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endsection

@section('script')
    <script src="{!! asset('theme/vendor/jquery-nice-select/js/jquery.nice-select.min.js') !!}"></script>
    <script src="{!! asset('theme/vendor/moment/moment.min.js') !!}"></script>
    <script src="{!! asset('theme/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}"></script>
    <script>
        $('#start, #end').bootstrapMaterialDatePicker({
            weekStart: 0,
            time: false
        });
    </script>
@endsection
